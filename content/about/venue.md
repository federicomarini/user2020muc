+++
title = "Venue"
description = "Details about location and setting"
keywords = ["location","travel","country","accommodation"]
+++

### <i class="fas fa-bed"></i> Stay at the Venue

The conference venue, the [Sheraton Munich Arabellapark Hotel](https://www.marriott.com/hotels/travel/mucsa-sheraton-munich-arabellapark-hotel/?scid=bb1a189a-fec3-4d19-a255-54ba596febe2) includes a conference center as well as a  hotel next door.

<br>

### <i class="fas fa-map-marker-alt"></i> Location

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d42590.24351177317!2d11.621166220556313!3d48.15092164295235!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xce646888d912e86a!2sSheraton%20Munich%20Arabellapark%20Hotel!5e0!3m2!1sde!2sde!4v1583314854017!5m2!1sde!2sde" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

<!-- go to google maps, select your location, and copy and paste the 'embed' frame into the file where you want it to appear -->
