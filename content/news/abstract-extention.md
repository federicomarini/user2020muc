+++
title = "Extended Abstract Submission Deadline"
date = "2020-03-16T08:00:00"
tags = ["Abstracts", "Submission"]
categories = ["Abstracts", "Submission"]
banner = "img/banners/abstract-extention-preview.jpg"
author = "Lea Vanheyden"
+++

The submission portal will stay open for another month, the new Abstract Submission Deadline is April 15th.
The reason for our decision is the current Corona crisis. We (and probably also you) need more time to plan the next steps. We will keep you posted.<!--more-->

This is an opportunity to share your work with other R enthusiasts! We strive to be a diverse event and welcome abstracts related to R from different backgrounds and career stages, whether your work is computational, empirical, or theoretical in nature. Everyone is encouraged to submit one or more abstracts.

For information on how the submission process works please see our [Call for Abstracts](../../../../2020/01/27/abstract-submission/).

