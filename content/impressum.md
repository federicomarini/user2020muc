+++
title = "Impressum"
description = "Impressum of the website"
keywords = ["impressum"]
+++

### Address

Department of Statistics <br>
Ludwig-Maximilians-Universität München <br>
Ludwigstr. 33 <br>
80539 München <br>

### Website

[Hugo website](https://gohugo.io/) using the [Universal theme](https://themes.gohugo.io/hugo-universal-theme/). Website is maintained by Daniel Schalk (daniel.schalk@stat.uni-muenchen.de).

### Contact

useR2020muc@R-project.org

<!-- **Someones phone number (maybe Giuseppe?, I don't have a fixed office)** -->

For specific inquiries we refer to the [organizing and programm committe](/contact_new/).
