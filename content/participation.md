+++
title = "Participation"
description = "Volunteering, Speaking and Attending"
keywords = ["tickets","call for papers","volunteer","organiser"]
id = "register"
+++

<br>

### <i class="fas fa-chalkboard-teacher"></i> Call for Tutorial Proposals

The call for proposals is now [live on our website](/../news/2020/01/13/tutorial-submission/) <!--and we have [begun accepting proposals](/news/2019/10/23/tutorial-submission-open/)-->! The submission will close on February 16th, 2020. <!-- We plan to notify tutorial applicants of acceptance in early January, prior to the opening of registration.-->

<br>

### <i class="fas fa-comment"></i> Call for Abstracts

The [submission portal](https://www.conftool.org/user2020muc) is now open! The deadline for abstracts will be March 15th, 2020. We will announce acceptances on April 30th, 2020.

You may now submit an abstract to present at useR!2020 European Hub in Munich. We welcome the following submission types:

- __Regular Talk__: Abstracts submitted for "regular talks" will be given consideration for regular talks, lightning talks, and posters. Please submit a title, abstract of up to 300 words, and author information as part of your submission. Selecting up to three Topics is required, Keywords are optional.

- __Lightning talk__: Abstracts submitted for "lightning talks" will be given consideration for lightning talks, and posters. Please submit a title, abstract of up to 300 words, and author information as part of your submission. Selecting up to three Topics is required, Keywords are optional.

- __Poster presentation__: Abstracts submitted for "poster presentations" will be given consideration for a poster presentation only. Please submit a title, abstract of up to 300 words, and author information as part of your submission. Selecting up to three Topics is required, Keywords are optional.

- __Breakout sessions__: Abstracts submitted for "breakout sessions" are open to any type of session. You can propose to organize a panel, a workshop, a discussion on a certain topic, an invited session of talks, or any other session you can imagine. Please submit a short title for the session, an abstract of up to 300 words, and author/participant/panelist information (if applicable) as part of your submission. If your session has specific participants/panelists, please indicate whether they have committed to participate in the session.

<br>

### <i class="fas fa-user-plus"></i> Registration

[Registration is now open!](https://www.conftool.org/user2020muc/index.php?page=login)

Early bird (regular)

-  Industry: 500 € (650 €)
-  Academics: 300 € (450 €)
-  Students/PhD students: 220 € (300 €)

Note: please contact us if you cannot pay the price assigned to your group or if it is not clear to you which group you belong to.

<!--

<br>

### <i class="fas fa-users"></i> Diversity Scholarships

As with past useR! conferences, we will be accepting applications for diversity scholarships, which will partially subsidize travel expenses and provide a full registration ticket (including two tutorials and the gala dinner events). There will also be a dedicated networking event for diversity scholarship recipients with the conference's sponsors. We anticipate the call for diversity scholarship applicants being posted on November 15th, 2019. The deadline for applications will be January 15th , 2020. We will open the portal for applications when the call is posted, and announce recipients on February 15th, 2020.

-->
