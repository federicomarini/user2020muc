+++
title = "useR! 2020 European Hub Accessibility FAQ"
description = "Frequently asked questions on accessibility"
keywords = ["FAQ","Accessibility"]
+++

  <p>useR! 2020 European Hub is committed to providing an inclusive environment
and we will do our best to accommodate requests for special assistance.
This page describes the accessibility features of useR! 2020 European Hub, to
help you make an informed decision about whether the conference will be
accessible to you. Please contact us if your question is not answered
here, or if the conference arrangements as described are not enough to
allow you to attend. We will work with you to the best of our ability,
to make the conference accessible.</p>
  
  <h2><a name="q">Contents</a></h2>
  
  <ul>
  <li><a href="#q1">How do I communicate accessibility needs to useR! 2020 European Hub organizers?</a></li>
  <li><a href="#q2">Who do I ask if my question is not answered here?</a></li>
  <li><a href="#q3">Will sign language interpretation or captioning be available?</a></li>
  <li><a href="#q4">What are the taxi and public transport options for getting to the conference
venue?</a></li>
  <li><a href="#q5">Who can I ask about accessibility in Munich, Germany?</a></li>
  <li><a href="#q6">Can a student volunteer assist me during the conference?</a></li>
  <li><a href="#q7">What is the conference space like?</a></li>
  <li><a href="#q8">Will the conference be accessible by wheelchair or power scooter?</a></li>
  <li><a href="#q9">What are the restroom facilities like?</a></li>
  <li><a href="#q10">What are the arrangements for presenters with accessibility needs?</a></li>
  <li><a href="#q11">Can I bring my guide dog?</a></li>
  <li><a href="#q12">Can I attend the conference by robot?</a></li>
  <li><a href="#q13">How much walking or standing will be needed?</a></li>
  <li><a href="#q14">Will there be an induction loop in the meeting rooms?</a></li>
  <li><a href="#q15">Will speakers and audience members asking questions be using a microphone?</a></li>
  <li><a href="#q16">What food service will be provided?</a></li>
  <li><a href="#q17">Can the conference accommodate special dietary needs?</a></li>
  <li><a href="#q18">Will the conference be a smoke-free environment?</a></li>
  <li><a href="#q20">Will there be loud music, strobe lighting, spotlights, or other strong sensory experiences?</a></li>
  <li><a href="#q21">Will there be a designated quiet space at the conference?</a></li>
  <li><a href="#q19">What if I have a medical emergency or health problem during the conference?</a></li>
  </ul>
  
  <br><br>
  
  <h2><a name="q1">How do I communicate accessibility needs to useR! 2020 European Hub organizers?</a></h2>
  
  <p>The Diversity-Equity-Inclusion Chairs for useR! 2020 European Hub is Tieu Binh Ly & Fabian Scheipl, whose goal is to ensure the conference is accessible to everyone. When you register for the conference, there will be a registration question labeled "Do you have a disability or special need which requires our awareness?". In your response, please indicate any accessibility needs such as wheelchair access, sign language interpretation, or a guide. The Diversity-Equity-Inclusion Chairs will follow up with you to clarify your needs. You can contact them directly at any time by emailing <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a>.</p>
  
  <h2><a name="q2">Who do I ask if my question is not answered here?</a></h2>
  
  <p>Please contact the conference hotel on <a href="tel:+49 89 9264 8205">+49 89 9264 8205</a> to discuss your specific needs with the venue directly.</p>
  
  <p>If there is specific accessibility information you would like to see here, or if you wish to discuss any conference accessibility requirements, please contact our Diversity-Equity-Inclusion Chairs, Tieu Binh Ly & Fabian Scheipl by email: <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a>, and someone will respond to you shortly.</p>
  
  <h2><a name="q3">Will sign language interpretation or captioning be available?</a></h2>
  
  <p>The conference will provide a team of sign language interpreters or captioners if requested by any attendees during the early registration period. Please make your request as early as possible. Our Diversity-Equity-Inclusion Chairs will follow up with you to discuss your needs in more detail. After May 1, 2020 (the deadline for early registration), a best effort will be made to accommodate requests, but we cannot guarantee that interpreters or captioners will be available.</p>
  
  <h2><a name="q4">What are the taxi and public transport options for getting to the conference venue?</a></h2>
  
  <p>From MUC Airport, the easiest way to get to the Sheraton München Arabellapark is Metro-Station "Arabellapark"</p>
  
<!--
  COMMENT: The description of each public transport option should include:
  Link to web page for accessibility information provided by the service itself.
Contact email, phone and TTY numbers for the service.
Where to find it in the airport.
How much it costs.
Whether the service is accessible for people using manual or larger powered wheelchairs.
Whether the service is accessible to a person using a larger electric scooter.
Whether wheelchair users need to call ahead to reserve.
Whether the service requires walking a long distance or up and down steps.
Whether service animals are accepted.
How long the journey typically takes.
Where the service drops passengers relative to the conference venue.
How to get from the dropping-off point to the conference venue in a wheelchair,
or without using steps, and how far it is to walk.
-->
  
  <p>Recommended options for getting to the conference include:</p>
  
  
  <h3><a href="https://www.mvg.de/ueber/engagement/barrierefreiheit.html">Underground</a></h3>
  
  <ul>
  <li><strong>Cost</strong>: ca. 10€</li>
  <li><strong>Time</strong>: ca. 45 min</li>
  <li><strong>Wheelchair accessibility</strong>: Small curbs (8-13cm), elevators available throughout.</li>
  <li><strong>Walking and steps</strong>: Walking distance from metro station approx. 400m.</li>
  <li><strong>Service animals</strong>: Allowed in metro trains.</li>
  <li><strong>Book</strong>: Please check directly with the vendor</li>
  </ul> 
  
  
  <h2><a name="q5">Who can I ask about accessibility in Munich, Germany?</a></h2>
  
  <p>For information about Munich, Germany accessibility, including public transport, go to <a href="https://www.muenchen-tourismus-barrierefrei.de/en/">Tourism Information of the Disabled Persons Advisory Commitee of the City of Munich</a>.</p>
  
  <h2><a name="q6">Can a student volunteer assist me during the conference?</a></h2>
  
  <p>Student volunteers will be available to assist attendees with disabilities with navigation, meal service, or other accessibility needs. If you will need volunteer assistance at the conference, please indicate this on your conference registration form.</p>
  
  
  <p>Attendees who require personal care assistance should bring their own assistant. Personal care assistants do not need to register for the conference. </p>
  
  <h2><a name="q7">What is the conference space like?</a></h2>
  
<!--
  COMMENT: This section should provide a map of the conference space that shows the following information:
  - the location of stairs, escalators, elevators, restrooms and wheelchair-accessible restrooms.
- walking distance between sessions and need to use stairs.
- places to sit and rest.
- whether there will be a quiet place where hard of hearing people can talk.
- whether there will be a private room where an attendee could rest or nurse a baby.
- where a guide dog can be taken to relieve itself.

COMMENT: For the benefit of those who cannot see the map, a textual description should also be included.
Here's an example of what this description might look like for a small event in a hotel:
  -->


  <p>We are still working on a description of the conference space.</p>

  <p>We are still working on an evacuation plan for attendees with mobility limitations.</p>

  <p>Please see our other FAQs for more details about <a href="#q8">wheelchair or power scooter access</a>, <a href="#q10">provisions for presenters</a>, <a href="#q9">restrooms</a>, <a href="#q13">walking and steps</a>, and <a href="#q11">facilities for assistance animals</a>.</p>

  <h2><a name="q8">Will the conference be accessible by wheelchair or power scooter?</a></h2>

<!--
      COMMENT: This section should provide information for wheelchair and power scooter users, including:
      - wheelchair access routes into the conference venue.
      - wheelchair route from the entrance or lobby to the meeting rooms and/or guest accommodation.
      - if venue is a hotel - wheelchair route from the hotel rooms to the meeting space.
      - location of wheelchair-accessible restrooms.
      - warnings about any steps or stairs between conference locations.
      - where to get more detailed information about accessibility at the venue.
      - if there will be a raised stage, will there be a ramp onto it?
      - wheelchair access and transportation for any offsite events.
  -->

  <p>
      Yes, the conference will be accessible to wheelchair users. We are still working on a detailed description of wheelchair access.
  </p>

  <p>We do not at this time have a ramp on to the stage. If you are a presenter who uses a wheelchair, please contact us as soon as possible so that we can make arrangements. After the early registration deadline we cannot guarantee to be able to provide a ramp. Poster sessions will have space for a wheelchair to move between the posters.</p>

  <p>We are still working on an evacuation plan for attendees with mobility limitations.</p>

  <p>For more information about wheelchair accessibility at Sheraton München Arabellapark, please contact the hotel on <a href="tel:+49 89 9264 8205">+49 89 9264 8205</a>.</p>


  <p>We are still gathering information on wheelchair access to the offsite events. Please contact us directly with any questions.</p>

  <h2><a name="q9">What are the restroom facilities like?</a></h2>

  <p>We are still working on a description of the restrooms.</p>

  <h2><a name="q10">What are the arrangements for presenters with accessibility needs?</a></h2>

  <p>We are still working on the stage design.</p>

  <p>We do not at this time have a ramp on to the stage. If you are a presenter who uses a wheelchair, please contact us as soon as possible so that we can make arrangements. After the early registration deadline we cannot guarantee to be able to provide a ramp. Poster sessions will have space for a wheelchair to move between the posters.</p>

  <p>If you would like a student volunteer to operate slides or guide you onto and off the stage, or have other accessibility requests please contact Tieu Binh Ly & Fabian Scheipl at <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a> by May 1, 2020.</p>

  <h2><a name="q11">Can I bring my guide dog?</a></h2>

  <p>Yes! Guide dogs and other service animals are welcome at useR! 2020 European Hub. There will be a relief area with water available. We are still working on the precise location. Please indicate on your registration form if you anticipate bringing a service animal to the conference.</p>

  <h2><a name="q12">Can I attend the conference by robot?</a></h2>


  <p>We are sorry, but useR! 2020 European Hub will not be able to accommodate remote attendance by robot.</p>

  <h2><a name="q13">How much walking or standing will be needed?</a></h2>

<!--
      COMMENT: This section should describe:
      - how far it is from the conference space to the elevator,
      - how far to the nearest accessible bathroom, and
      - mention any ramp that is needed to move between parts of the space.
  -->

  <p>We are still working on our description of the walking demands of the conference.</p>
  <p>We are still gathering information on walking demands of the offsite events.</p>

  <p>Please contact Tieu Binh Ly & Fabian Scheipl at <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a> as early as possible if you have further questions or special requests.</p>

  <h2><a name="q14">Will there be an induction loop in the meeting rooms?</a></h2>

  <p>
      If required by a sufficiently large number of participants, we will be able to provide induction loop coverage in the CUVILLIES 1 & 2 rooms.
       Rooms Asam 1 and Asam 2 will not provide induction loop coverage.
  </p>

  <h2><a name="q15">Will speakers and audience members asking questions be using a microphone?</a></h2>

  <p>
      Yes, microphones will be used for all speakers.
      Attendees with questions will be requested to use a microphone so that their question is more easily heard.
  </p>

  <h2><a name="q16">What food service will be provided?</a></h2>

  <p>The conference will provide Buffet lunches and refreshments at breaks.. Dishes will be labeled. For more detailed information please contact TBD at <a href="mailto:TBD">TBD</a>. If you have special dietary needs, please indicate these clearly on your registration form.</p>

  <h2><a name="q17">Can the conference accommodate special dietary needs?</a></h2>

  <p>Yes. Attendees who indicate special dietary requirements on their registration form will be provided with special meals when the conference catering does not accommodate them. If you have a severe allergy please indicate this on your registration form. For more detailed information please contact TBD at <a href="mailto:TBD">TBD</a>.</p>

  <h2><a name="q18">Will the conference be a smoke-free environment?</a></h2>

  <p>
    The entire conference venue is a no-smoking area.
    We are still gathering information on where there are smoking areas around the venue.
  </p>

  <h2><a name="q20">Will there be loud music, strobe lighting, spotlights, or other strong sensory experiences?</a></h2>

  <p>We are still gathering information on sensory aspects of the conference. Participants will be requested not to use flash photography or to wear strong scents. Please indicate on your registration form if there are specific items you would like to know about in advance.</p>

  <p>We are still gathering information on sensory demands of the offsite events and availability of quiet places.</p>

  <p>Please contact Tieu Binh Ly & Fabian Scheipl at <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a> as early as possible if you have further questions or special requests.</p>


  <h2><a name="q21">Will there be a designated quiet space at the conference?</a></h2>

  <p>We hope to be able to provide a quiet space at the conference but are still checking on availability. Please contact us if you are planning to attend the conference and may need such a space.</p>

  <h2><a name="q19">What if I have a medical emergency or health problem during the conference?</a></h2>

  <p>In case of emergency call <a href="tel:112">112</a> for an ambulance. Hospital "Klinikum Bogenhausen" is only a couple of 100m from the venue. If you require non-urgent medical care, please contact Tieu Binh Ly & Fabian Scheipl at <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a> or ask any student volunteer.</p>

<br><br>

  <hr>
  <p>This FAQ has been generated using the <a href="http://www.sigaccess.org/accessibility-faq-page-generator-2/">Accessibility FAQ Generator</a> tool provided by <a href="http://www.sigaccess.org/">ACM SIGACCESS</a>. Event organizers are welcome to modify and reuse this template for other events. Please contact SIGACCESS for more information.</p>